--Name of Database
CREATE DATABASE coursebookingdb;


--Teachers
CREATE TABLE teachers(

	teacher_id INT NOT NULL AUTO_INCREMENT,
	teacher_name VARCHAR(50) NOT NULL,
	PRIMARY KEY (teacher_id)
);


--Students
CREATE TABLE students(

	student_id INT NOT NULL AUTO_INCREMENT,
	student_name VARCHAR(50) NOT NULL,
	PRIMARY KEY (student_id)
);


--Courses
CREATE TABLE courses(

	course_id INT NOT NULL AUTO_INCREMENT,
	teacher_id INT NOT NULL,
	course_name VARCHAR(50) NOT NULL,
    PRIMARY KEY (course_id),
    CONSTRAINT fk_courses_teacher_id
    	FOREIGN KEY (teacher_id) REFERENCES teachers(teacher_id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


--Student_courses
CREATE TABLE student_courses(

	id INT NOT NULL AUTO_INCREMENT,
	course_id INT NOT NULL,
	student_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_courses_courses_id
    	FOREIGN KEY (course_id) REFERENCES courses(course_id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT,
	CONSTRAINT fk_students_student_id
    	FOREIGN KEY (student_id) REFERENCES students(student_id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT

);
